# Views Header / Footer Add Node Link

This is a simple, light-weight module that creates a global option to add a
"+ _content-type_" link to the header or footer area of a view.

The use case is when you have a listing of a particular content type, and in the
header or footer area you'd like to have an easy link for the user to click to
add an instance of the content type after they didn't find what they were
looking for in the view - so for example, a listing of inventory items, and
after not finding the one the user wanted, they could simply click the
"+ Inventory Item" link at the top or bottom (or both) of the view, which will
take them to the form found at `node/add/content-type'.

In the case of an  EVA display, where the view is attached to a node display
such as when you might be showing content related to the displayed node via an
entity-reference field, the displaying nid is passed to the node-add form via
the query part of the URL in the form `content_type=nid'.  The use case here is
that you can then take the passed nid to default the entity-reference field on
the node-add form to the nid of the node from which you wanted to add more of
the child content.

The module checks whether or not the current user has permissions to add content
of the specified type, and if not, nothing is displayed.

The global area has a number of configuration options from which to chose,
including the content type, alternate text if you don't want to use the content
type's name, something other than a plus sigh (+) for the prefix, and a number
of styling options for the link and its wrapper.

## Installation

Install this module as you would any other (usually in sites/all/modules), and
then simply activate it.  The option will then appear in your views UI.

## Credits

Thanks to [agentrickard](https://www.drupal.org/u/agentrickard) for the
[Views link area](https://www.drupal.org/project/views_linkarea) module, from
which I pulled invaluable guidance about how to build this module.
