<?php

class views_handler_area_hf_add_node_link extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();
    $options['type'] = array('default' => '');
    $options['prefix'] = array('default' => '+', 'translatable' => TRUE);
    $options['text'] = array('default' => '', 'translatable' => TRUE);
    $options['post_prefix_space'] = array('default' => '&nbsp;');
    $options['return'] = array('default' => TRUE);
    $options['default_classes'] = array('default' => TRUE);
    $options['class'] = array('default' => '');
    $options['wrapper'] = array('default' => 'p');
    $options['wrapper_class'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('Content type'),
      '#options' => node_type_get_names(),
      '#default_value' => $this->options['type'],
      '#description' => t('Select which content type the "create new" form for which should be opened by this link.'),
      '#required' => TRUE,
    );
    $form['prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Prefix'),
      '#default_value' => $this->options['prefix'],
      '#description' => t('The prefix to show in front of the text of the link.  If left empty, the plus sign (+) will be used.  '
              . 'The prefix will be surrounded by a &lt;span&gt; with a class <em>add-node-link-prefix</em> so it can be stiled separately from the rest of the link,'),
    );
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Link text'),
      '#default_value' => $this->options['text'],
      '#description' => t('The text of the link.  If left empty, the content type name will be used.'),
    );
    $form['post_prefix_space'] = array(
      '#type' => 'radios',
      '#title' => t('Non-breaking space between the prefix and the link text'),
      '#default_value' => $this->options['post_prefix_space'],
      '#options' => array(
        '' => t('No space'),
        ' ' => t('Regular space'),
        '&nbsp;' => t('Non-breaking space'),
      ),
      '#description' => t('Select what you would like between the prefix and the link text.'),
    );
    $form['return'] = array(
      '#type' => 'checkbox',
      '#title' => t('Set destination as the current page'),
      '#default_value' => $this->options['return'],
      '#description' => t('This will redirect the user back to the view when the form is submitted.'),
    );
    $form['default_classes'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add default link classes'),
      '#default_value' => $this->options['default_classes'],
      '#description' => t('Check this box to add the default classes of "add-node-link" and "add-<em>content-type</em>-link" to the link classes.'),
    );
    $form['class'] = array(
      '#type' => 'textfield',
      '#title' => t('Link CSS class(es)'),
      '#default_value' => $this->options['class'],
      '#description' => t('A custom CSS class to add to the link.  Separate classes with a space.'),
      '#fieldset' => 'attributes',
    );
    $form['wrapper'] = array(
      '#type' => 'select',
      '#size' => 1,
      '#title' => t('HTML wrapper'),
      '#default_value' => $this->options['wrapper'],
      '#options' => array(
        '' => t('None'),
        'p' => '<p>',
        'span' => '<span>',
        'div' => '<div>',
        'h2' => '<h2>',
        'h3' => '<h3>',
        'h4' => '<h4>',
        'h5' => '<h5>',
        'h6' => '<h6>',
      ),
      '#description' => t('Customise the HTML around the link.'),
    );
    $form['wrapper_class'] = array(
      '#type' => 'textfield',
      '#title' => t('HTML wrapper class(es)'),
      '#default_value' => $this->options['wrapper_class'],
      '#description' => t('Specify any class(es) you want to be added to the wrapper element.  Separate classes with a space.'),
    );
  }

  function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);
  }

  function render($empty = FALSE) {

    if ($empty && empty($this->options['empty'])) {
      return '';
    }
    // Check that the user has access to create the content type.
    if ( !user_access('create ' . $this->options['type'] . ' content') ) {
      return '';
    }

    // Determine the text for the link.
    if ( empty($this->options['text']) ) {
      $node_type = node_type_get_type($this->options['type']);
      $text = $node_type->name;
    }
    else {
      $text = $this->options['text'];
    }

    // Set up various link properties.
    $link_properties = array();
    // Using HTML for the link to ensure the prefix is part of the link.
    $link_properties['html'] = TRUE;
    $text = check_plain($text);

    // Set up a from parameter on the URL if this is an EVA display.
    if ( is_object($this->view->display_handler) && is_a($this->view->display_handler, 'eva_plugin_display_entity') ) {
      // Identify the bundle type so the node-add form knows from what node-type it was called, so the appropriate
      // default value for the entity-reference field can be set.
      $from = !empty($this->view->display[$this->view->current_display]->display_options['bundles'][0])
          ? $this->view->display[$this->view->current_display]->display_options['bundles'][0]
          : NULL;
      // If this is being called from an EVA display, then pass the parameter to the view in the query string for the node/add form to use.
      if ( !empty($this->view->args[0]) && !empty($from) ) {
        $link_properties['query'][$from] = $this->view->args[0];
      }
    }

    if (!empty($this->options['return'])) {
      $destination = drupal_get_destination();
      $link_properties['query']['destination'] = $destination['destination'];
    }

    // Set the bubble-help for the link.
    $link_properties['attributes'] = array('title' => t('Click here to add another !name', array('!name' => $text)));

    // If the user has specified custom classes, sanitise and then add them.
    if ( !empty($this->options['class']) ) {
      $link_properties['attributes']['class'] = explode(' ', preg_replace('/[^a-z -]/i', '', strip_tags($this->options['class'])));
    }

    // Add the default link classes if user has said to do so.
    if ( !empty($this->options['default_classes']) ) {
      $link_properties['attributes']['class'][] = 'add-node-link';
      $link_properties['attributes']['class'][] = 'add-' . drupal_html_class($this->options['type']) . '-link';
    }

    // Handle multi-name node types for the node/add URL.
    $url_type = str_replace('_', '-', $this->options['type']);

    // If the user has specified a wrapper, set it up along with any sanitised wrapper classes.
    if ( !empty($this->options['wrapper']) ) {
      $classes = preg_replace('/[^a-z -]/i', '', strip_tags($this->options['wrapper_class']));
      $open = '<' . $this->options['wrapper'] . ( !empty($classes) ? ' class="' . $classes. '"' : '' ) . '>';
      $close = '</' . $this->options['wrapper'] . '>';
    }
    else {
      $open = $close = '';
    }

    // Prepare the prefix.
    $specified_prefix = strip_tags($this->options['prefix']);
    $prefix = '<span class="add-node-link-prefix">' . ( !empty($specified_prefix) ? $specified_prefix : '+' ) . '</span>' .
            $this->options['post_prefix_space'];

    // Return the link.
    return $open . l($prefix . $text, "node/add/$url_type", $link_properties) . $close;
  }

}
