<?php

/**
 * Implementation of hook_views_data()
 */
function views_hf_add_node_link_views_data() {
  $data = array();
  $data['views']['add_node_link'] = array(
    'title' => t('Add node link'),
    'help' => t('Provide a link to add a node of configurable type.'),
    'area' => array(
      'handler' => 'views_handler_area_hf_add_node_link',
    ),
  );
  return $data;
}
